# Practice using Vite and Tailwind-CSS

The project was created for an exercise focused on React base on Vite and Tailwind CSS

## Source

Tailwind CSS: https://tailwindcss.com/ \
Vite: https://vitejs.dev/

## TO-DO

- [ ] Add user detail setting page.
- [ ] Add backend validation for existing mail and username.
- [ ] Add mobile version modification.
- [ ] Animation and Transition.

-- Just do more and more to enhance understanding of UI and UX

## Available Scripts

### `npm run dev`

Runs the app in the development mode

### `npm run build`

Builds the app for production
