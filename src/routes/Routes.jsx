import React from "react";
import { Route, Routes } from "react-router-dom";
import Greetings from "../page/Greetings";
import Profile from "../page/Profile";
import Register from "../page/Register";
import Layout from "./Layout";

const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route path="/" element={<Register />} />
        <Route path="/greetings" element={<Greetings />} />
        <Route path="/profile" element={<Profile />} />
      </Route>
    </Routes>
  );
};

export default Routers;
