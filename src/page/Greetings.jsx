import React from "react";
import { useUser } from "../context/UserContext";

const Greetings = () => {
  const { user } = useUser();

  return (
    <>
      {user?.username && (
        <div className="grid place-items-center w-screen h-screen dark:bg-slate-800">
          <div className="flex items-center px-10 bg-slate-400 min-h-[120px] rounded-2xl ring-1 ring-offset-1 ring-slate-900 ring-offset-slate-900 shadow-lg shadow-slate-900">
            <p className="text-lg ">
              Greeting's to our new member{" "}
              <span className="font-bold">Sir {user.username}</span>
            </p>
          </div>
        </div>
      )}
    </>
  );
};

export default Greetings;
