import React, { useState } from "react";

const Profile = () => {
  const [showDetails, setShowDetails] = useState(false);

  const handleShowDetails = () => {
    setShowDetails((prev) => !prev);
  };

  return (
    <div className="container mx-auto">
      <div className="flex flex-col p-6 px-2 max-w-sm mx-auto bg-slate-700 rounded-2xl ring-1 ring-offset-1 ring-slate-900 ring-offset-slate-900 shadow-lg shadow-slate-900 relative">
        <div className="flex gap-6 mx-7">
          <div className="w-16 h-16 rounded-full flex border-solid border-2 border-slate-900 shadow-md shadow-slate-900">
            <span className="flex items-center mx-auto">🦾</span>
          </div>
          <div className="flex flex-col justify-between start">
            <h6 className="text-slate-400 -mt-[3px]">Abayabaya</h6>
            <h6 className="text-slate-400 -mt-[2px] mb-[5px]">
              abayabaya@gmail.com
            </h6>
            <h6 className="text-[12px] text-slate-500">ID : </h6>
          </div>
        </div>
        {showDetails && (
          <div className="p-7 pb-1">
            <p>details</p>
          </div>
        )}
        <div
          onClick={handleShowDetails}
          className="w-7 h-7 rounded-full absolute -bottom-5 left-1/2 -translate-x-1/2 hover:border-2 hover:bg-slate-900 hover:bg-opacity-70 border-slate-900 group"
        >
          <p className="text-center text-xs font-semibold text-slate-300 group-hover:text-slate-400 cursor-pointer">
            {showDetails ? "N" : "V"}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Profile;
