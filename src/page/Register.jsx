// library imports
import React, { useReducer } from "react";
import { useNavigate } from "react-router-dom";
import { useUser } from "../context/UserContext";

const EMAIL_REGEX =
  /^([a-zA-Z0-9_]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
const USER_REGEX = /^[A-z][A-z0-9-_]{5,25}$/;
const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,32}$/;

const setRegState = (state, action) => {
  switch (action.type) {
    case "setField": {
      return {
        ...state,
        [action.field]: action.value,
        ...(action.validation && {
          [action.validation]: action.regex?.test(action.value),
        }),
      };
    }
    default:
      return state;
  }
};

const regState = {
  email: "",
  validEmail: "",
  username: "",
  validUsername: "",
  password: "",
  validPassword: "",
  rePassword: "",
};

const inputStyle = (field, validation) => {
  const baseStyle =
    "peer w-full p-1.5 text-sm outline-1 bg-white rounded-sm outline-none placeholder-slate-400 focus:outline-none focus:ring-1";
  const darkStyle =
    "dark:focus:outline-none dark:outline-2 dark:focus:ring-1 dark:bg-slate-900 dark:text-slate-200 dark:placeholder-slate-600";
  const disabledStyle =
    "disabled:text-slate-400 disabled:outline-1 disabled:outline-slate-300 disabled:bg-slate-300 disabled:placeholder-slate-400 disabled:rounded-sm";
  const darkDisableStyle =
    "dark:disabled:text-slate-600 dark:disabled:outline-1 dark:disabled:outline-slate-700 dark:disabled:bg-slate-900 dark:disabled:placeholder-slate-800";
  const mergeStyle = `${baseStyle} ${darkStyle} ${disabledStyle} ${darkDisableStyle}`;

  if (field.length <= 0)
    return (
      mergeStyle +
      " outline-slate-700 dark:outline-slate-500 focus:ring-slate-700 dark:focus:ring-slate-100 focus:ring-offset-2 dark:focus:ring-offset-0"
    );

  const addStyle = !validation
    ? " outline-pink-700 text-pink-700 focus:ring-pink-700 focus:ring-offset-2 dark:text-pink-600 dark:focus:ring-pink-700 dark:focus:ring-offset-pink-900"
    : " outline-sky-600 text-sky-600 focus:ring-sky-600 focus:ring-offset-2 dark:outline-emerald-600 dark:text-emerald-400 dark:focus:ring-emerald-700 dark:focus:ring-offset-emerald-600";
  return mergeStyle + addStyle;
};

function Register() {
  const [registerState, dispatch] = useReducer(setRegState, regState);
  const { setUser } = useUser();
  const navigate = useNavigate();

  const validStep = {
    second: registerState.validEmail && registerState.validUsername,
    third:
      registerState.validEmail &&
      registerState.validUsername &&
      registerState.validPassword,
    final:
      registerState.validEmail &&
      registerState.validUsername &&
      registerState.validPassword &&
      registerState.password === registerState.rePassword,
  };

  const invalidSteps =
    (!registerState.validEmail && registerState.email) ||
    (!registerState.validUsername && registerState.username) ||
    (!registerState.validPassword && registerState.password) ||
    (registerState.password !== registerState.rePassword &&
      registerState.rePassword);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.table(registerState);
    setUser((e) => ({
      ...e,
      username: registerState.username,
      email: registerState.email,
    }));
    navigate("/greetings", { replace: true });
  };

  const handleChange = (
    e,
    field,
    validation = undefined,
    regex = undefined
  ) => {
    dispatch({
      type: "setField",
      field,
      value: e.currentTarget.value,
      validation,
      regex,
    });
  };

  return (
    <div className="grid place-items-center w-screen h-screen dark:bg-slate-800">
      <div
        className={`grid place-items-center p-10 pt-7 rounded-lg max-w-lg bg-slate-200 dark:bg-slate-900 border-2 ${
          !registerState.email
            ? "dark:border-slate-600 border-slate-400"
            : invalidSteps
            ? "dark:border-pink-900 border-pink-600"
            : "dark:border-emerald-900 border-emerald-400"
        }`}
      >
        <h4
          className={`text-center text-lg text-slate-800 dark:text-slate-200 font-semibold pb-10 pt-3 ${
            !registerState.email
              ? "dark:text-slate-200"
              : invalidSteps
              ? "dark:text-pink-700 text-pink-600"
              : "dark:text-emerald-500 text-emerald-700"
          }`}
        >
          Create Account
        </h4>
        <form
          onSubmit={handleSubmit}
          className="flex flex-col gap-5 min-w-[200px] max-w-min"
        >
          <label htmlFor="email" className="block">
            <input
              id="email"
              className={`${inputStyle(
                registerState.email,
                registerState.validEmail
              )}`}
              type="text"
              placeholder="Your email"
              value={registerState.email}
              onChange={(e) => {
                handleChange(e, "email", "validEmail", EMAIL_REGEX);
              }}
            ></input>
            {!registerState.validEmail && registerState.email && (
              <>
                <p className="text-pink-700 text-xs mt-1 peer-disabled:hidden text-justify">
                  Invalid email format, make sure to use your correct email
                </p>
                <p className="text-pink-700 text-xs peer-disabled:hidden text-justify">
                  example: example@exam.ex
                </p>
              </>
            )}
          </label>

          <label htmlFor="username" className="block">
            <input
              id="username"
              className={inputStyle(
                registerState.username,
                registerState.validUsername
              )}
              type="text"
              placeholder="Your username"
              disabled={!registerState.validEmail}
              value={registerState.username}
              onChange={(e) => {
                handleChange(e, "username", "validUsername", USER_REGEX);
              }}
            ></input>
            {!validStep.second && registerState.username && (
              <p className="text-pink-700 text-xs mt-1 peer-disabled:hidden text-justify">
                Username can only consist letter and number with length of 6-24
              </p>
            )}
          </label>
          <label htmlFor="password" className="block">
            <input
              id="password"
              className={inputStyle(
                registerState.password,
                registerState.validPassword
              )}
              type="password"
              placeholder="Set password"
              disabled={!validStep.second}
              value={registerState.password}
              onChange={(e) => {
                handleChange(e, "password", "validPassword", PWD_REGEX);
              }}
            ></input>
            {!validStep.third && registerState.password && (
              <p className="text-pink-700 text-xs mt-1 peer-disabled:hidden text-justify">
                Password should consist Uppercase, Lowercase, and Number with
                length of 8-32
              </p>
            )}
          </label>
          <label htmlFor="re-password" className="block">
            <input
              id="re-password"
              className={inputStyle(
                registerState.rePassword,
                registerState.password === registerState.rePassword
              )}
              type="password"
              placeholder="Validate password"
              disabled={!validStep.third}
              value={registerState.rePassword}
              onChange={(e) => {
                handleChange(e, "rePassword");
              }}
            ></input>
            {!validStep.final && registerState.rePassword && (
              <p className="text-pink-700 text-xs mt-1 peer-disabled:hidden text-justify">
                Password doesn't match, make sure to use the correct password
              </p>
            )}
          </label>
          <button
            className="mt-9 text-sm p-2 py-2.5 rounded-md font-medium disabled:font-normal bg-sky-600 hover:bg-sky-500 text-white disabled:bg-slate-400 disabled:text-slate-500 disabled:hover:text-slate-500 disabled:hover:bg-slate-400 disabled:hover:font-normal dark:bg-emerald-600 dark:hover:bg-emerald-500 dark:hover:text-slate-900 dark:hover:font-extrabold"
            disabled={!validStep.final}
          >
            Let's Go!
          </button>
        </form>
      </div>
    </div>
  );
}

export default Register;
