// component imports
import Routers from "./routes/Routes";

function App() {
  return (
    <div className="w-screen h-screen dark:bg-slate-800 pt-5">
      <Routers />
    </div>
  );
}

export default App;
